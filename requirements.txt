# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: linux-64
_libgcc_mutex=0.1=conda_forge
_openmp_mutex=4.5=2_kmp_llvm
abseil-cpp=20211102.0=h27087fc_1
absl-py=1.4.0=pypi_0
aiohttp=3.8.4=py38h1de0b5d_0
aiohttp-cors=0.7.0=pypi_0
aiorwlock=1.3.0=pypi_0
aiosignal=1.3.1=pyhd8ed1ab_0
anyio=3.6.2=pypi_0
appdirs=1.4.4=pyhd3eb1b0_0
arrow=1.2.3=pyhd8ed1ab_0
arrow-cpp=10.0.1=py38h613000e_0
asttokens=2.2.1=pypi_0
async-timeout=4.0.2=pyhd8ed1ab_0
attrs=22.2.0=pyh71513ae_0
aws-c-common=0.4.57=he6710b0_1
aws-c-event-stream=0.1.6=h2531618_5
aws-checksums=0.1.9=he6710b0_0
aws-sdk-cpp=1.8.185=hce553d0_0
backcall=0.2.0=pypi_0
beautifulsoup4=4.12.2=pypi_0
binaryornot=0.4.4=py_1
binutils_impl_linux-64=2.40=hf600244_0
blessed=1.20.0=pypi_0
boost-cpp=1.81.0=he95ae9e_0
bootstrapped=0.0.2=pypi_0
brotlipy=0.7.0=py38h27cfd23_1003
bs4=0.0.1=pypi_0
bzip2=1.0.8=h7b6447c_0
c-ares=1.18.1=h7f8727e_0
ca-certificates=2022.12.7=ha878542_0
cachetools=5.3.0=pypi_0
certifi=2022.12.7=pyhd8ed1ab_0
cffi=1.15.1=py38h74dc2b5_0
chardet=5.1.0=py38h578d9bd_0
charset-normalizer=2.0.4=pyhd3eb1b0_0
click=8.1.3=unix_pyhd8ed1ab_2
cmake=3.25.2=h077f3f9_0
codecarbon=2.2.5=pypi_0
colorful=0.5.5=pypi_0
contourpy=1.1.0=pypi_0
cookiecutter=2.1.1=pyh6c4a22f_0
cryptography=38.0.4=py38h9ce1e76_0
cudatoolkit=11.3.1=h2bc3f7f_2
cudnn=8.4.1.50=hed8a83a_0
cycler=0.11.0=pypi_0
dataclasses=0.8=pyhc8e2a94_3
datasets=2.11.0=pyhd8ed1ab_0
decorator=5.1.1=pypi_0
deepdiff=6.3.1=pypi_0
dill=0.3.6=pyhd8ed1ab_1
distlib=0.3.6=pypi_0
docker-pycreds=0.4.0=pypi_0
evaluate=0.2.2=pyhd8ed1ab_0
executing=1.2.0=pypi_0
expat=2.5.0=h27087fc_0
experiment-impact-tracker=0.1.8=pypi_0
fastapi=0.92.0=pypi_0
filelock=3.9.0=pypi_0
flit-core=3.6.0=pyhd3eb1b0_0
fonttools=4.41.0=pypi_0
frozenlist=1.3.3=py38h0a891b7_0
fsspec=2023.1.0=pypi_0
future=0.18.3=pypi_0
fuzzywuzzy=0.18.0=pypi_0
gcc=12.2.0=h26027b1_11
gcc_impl_linux-64=12.2.0=hcc96c02_19
geocoder=1.38.1=pypi_0
geographiclib=2.0=pypi_0
geopy=2.3.0=pypi_0
gflags=2.2.2=he1b5a44_1004
gitdb=4.0.10=pypi_0
gitpython=3.1.32=pypi_0
glog=0.5.0=h48cff8f_0
google-api-core=2.11.0=pypi_0
google-auth=2.16.0=pypi_0
google-auth-oauthlib=0.4.6=pypi_0
googleapis-common-protos=1.58.0=pypi_0
gpustat=1.0.0=pypi_0
grpc-cpp=1.46.1=h33aed49_1
grpcio=1.51.1=pypi_0
gxx=12.2.0=h26027b1_11
gxx_impl_linux-64=12.2.0=hcc96c02_19
h11=0.14.0=pypi_0
huggingface-hub=0.12.1=pypi_0
huggingface_hub=0.13.3=pyhd8ed1ab_0
icu=70.1=h27087fc_0
idna=3.4=py38h06a4308_0
importlib-metadata=6.0.0=pypi_0
importlib-resources=5.10.2=pypi_0
importlib_metadata=6.1.0=hd8ed1ab_0
intel-openmp=2022.1.0=h9e868ea_3769
ipython=8.12.2=pypi_0
jedi=0.18.2=pypi_0
jinja2=3.1.2=pyhd8ed1ab_1
jinja2-time=0.2.0=pyhd8ed1ab_3
joblib=1.1.1=py38h06a4308_0
jsonschema=4.17.3=pypi_0
kernel-headers_linux-64=2.6.32=he073ed8_15
kiwisolver=1.4.4=pypi_0
krb5=1.19.4=h568e23c_0
ld_impl_linux-64=2.40=h41732ed_0
libblas=3.9.0=16_linux64_mkl
libbrotlicommon=1.0.9=h166bdaf_8
libbrotlidec=1.0.9=h166bdaf_8
libbrotlienc=1.0.9=h166bdaf_8
libcblas=3.9.0=16_linux64_mkl
libcurl=7.87.0=h91b91d3_0
libedit=3.1.20221030=h5eee18b_0
libev=4.33=h7f8727e_1
libevent=2.1.10=h9b69904_4
libffi=3.3=he6710b0_2
libgcc-devel_linux-64=12.2.0=h3b97bd3_19
libgcc-ng=12.2.0=h65d4601_19
libgfortran-ng=12.2.0=h69a702a_19
libgfortran5=12.2.0=h337968e_19
libgomp=12.2.0=h65d4601_19
liblapack=3.9.0=16_linux64_mkl
libnghttp2=1.46.0=hce63b2e_0
libprotobuf=3.20.3=he621ea3_0
libsanitizer=12.2.0=h46fd767_19
libssh2=1.10.0=h8f2d780_0
libstdcxx-devel_linux-64=12.2.0=h3b97bd3_19
libstdcxx-ng=12.2.0=h46fd767_19
libthrift=0.15.0=he6d91bd_1
libuv=1.40.0=h7b6447c_0
libzlib=1.2.13=h166bdaf_4
lightning-utilities=0.8.0=pyhd8ed1ab_0
llvm-openmp=14.0.6=h9e868ea_0
lz4-c=1.9.4=h6a678d5_0
magma=2.5.4=hc72dce7_4
markdown=3.4.1=pypi_0
markupsafe=2.1.2=py38h1de0b5d_0
matplotlib=3.7.2=pypi_0
matplotlib-inline=0.1.6=pypi_0
mkl=2022.1.0=hc2b9512_224
msgpack=1.0.4=pypi_0
multidict=6.0.4=py38h1de0b5d_0
multiprocess=0.70.14=py38h0a891b7_3
nccl=2.14.3.1=h0800d71_0
ncurses=6.4=h6a678d5_0
ninja=1.10.2=h06a4308_5
ninja-base=1.10.2=hd09550d_5
numpy=1.23.5=py38h7042d01_0
nvidia-ml-py=11.495.46=pypi_0
oauthlib=3.2.2=pypi_0
opencensus=0.11.1=pypi_0
opencensus-context=0.1.3=pypi_0
openssl=1.1.1t=h0b41bf4_0
orc=1.7.4=hb3bc3d3_1
ordered-set=4.1.0=pypi_0
packaging=22.0=py38h06a4308_0
pandas=1.5.3=pypi_0
parso=0.8.3=pypi_0
pathtools=0.1.2=pypi_0
pexpect=4.8.0=pypi_0
pickleshare=0.7.5=pypi_0
pillow=10.0.0=pypi_0
pip=22.3.1=py38h06a4308_0
pkgutil-resolve-name=1.3.10=pypi_0
platformdirs=3.0.0=pypi_0
pooch=1.4.0=pyhd3eb1b0_0
progiter=2.0.0=pypi_0
prometheus-client=0.13.1=pypi_0
prompt-toolkit=3.0.39=pypi_0
protobuf=3.20.3=pypi_0
psutil=5.9.4=pypi_0
ptyprocess=0.7.0=pypi_0
pure-eval=0.2.2=pypi_0
py-cpuinfo=9.0.0=pypi_0
py-spy=0.3.14=pypi_0
pyarrow=7.0.0=pypi_0
pyasn1=0.4.8=pypi_0
pyasn1-modules=0.2.8=pypi_0
pycparser=2.21=pyhd3eb1b0_0
pydantic=1.10.5=pypi_0
pygments=2.15.1=pypi_0
pylatex=1.4.1=pypi_0
pynvml=11.5.0=pypi_0
pyopenssl=22.0.0=pyhd3eb1b0_0
pyparsing=3.0.9=pypi_0
pyrsistent=0.19.3=pypi_0
pysocks=1.7.1=py38h06a4308_0
python=3.8.11=h12debd9_0_cpython
python-box=7.0.0=py38h1de0b5d_0
python-dateutil=2.8.2=pyhd8ed1ab_0
python-slugify=8.0.1=pyhd8ed1ab_0
python-tzdata=2023.3=pyhd8ed1ab_0
python-xxhash=3.2.0=py38h1de0b5d_0
python_abi=3.8=3_cp38
pytorch=1.11.0=cuda112py38habe9d5a_1
pytorch-gpu=1.11.0=cuda112py38h68407e5_1
pytorch-lightning=2.0.1.post0=pyhd8ed1ab_0
pytz=2022.7.1=pypi_0
pyyaml=6.0=py38h0a891b7_5
ratelim=0.1.6=pypi_0
ray=2.5.1=pypi_0
re2=2022.04.01=h27087fc_0
readline=8.2=h5eee18b_0
regex=2022.10.31=pypi_0
requests=2.28.1=py38h06a4308_0
requests-oauthlib=1.3.1=pypi_0
responses=0.18.0=pyhd8ed1ab_0
rhash=1.4.1=h3c74f83_1
rsa=4.9=pypi_0
ruamel.yaml=0.17.21=py38h1de0b5d_3
ruamel.yaml.clib=0.2.7=py38h1de0b5d_1
sacremoses=0.0.53=pyhd8ed1ab_0
scikit-learn=1.2.0=py38h6a678d5_0
scipy=1.10.0=py38h10c12cc_2
seaborn=0.12.2=pypi_0
sentencepiece=0.1.95=py38hd09550d_0
sentry-sdk=1.28.1=pypi_0
seqeval=1.2.2=pyhd3deb0d_0
setproctitle=1.3.2=pypi_0
setuptools=65.6.3=py38h06a4308_0
shapely=2.0.1=pypi_0
six=1.16.0=pyh6c4a22f_0
sleef=3.5.1=h9b69904_2
smart-open=6.3.0=pypi_0
smmap=5.0.0=pypi_0
snappy=1.1.10=h9fff704_0
sniffio=1.3.0=pypi_0
soupsieve=2.4.1=pypi_0
sqlite=3.40.1=h5082296_0
stack-data=0.6.2=pypi_0
starlette=0.25.0=pypi_0
sysroot_linux-64=2.12=he073ed8_15
tabulate=0.9.0=pypi_0
tensorboard=2.12.0=pypi_0
tensorboard-data-server=0.7.0=pypi_0
tensorboard-plugin-wit=1.8.1=pypi_0
tensorboardx=2.6=pypi_0
text-unidecode=1.3=py_0
threadpoolctl=2.2.0=pyh0d69192_0
tk=8.6.12=h1ccaba5_0
tokenizers=0.13.2=pypi_0
toml=0.10.2=pyhd8ed1ab_0
torchcrf=1.1.0=pypi_0
torchmetrics=0.11.4=pyhd8ed1ab_0
tqdm=4.64.1=py38h06a4308_0
traitlets=5.9.0=pypi_0
transformers=4.26.1=pypi_0
typing-extensions=4.4.0=py38h06a4308_0
typing_extensions=4.4.0=py38h06a4308_0
ujson=5.8.0=pypi_0
unidecode=1.3.6=pyhd8ed1ab_0
urllib3=1.26.14=py38h06a4308_0
utf8proc=2.6.1=h27cfd23_0
uvicorn=0.20.0=pypi_0
virtualenv=20.19.0=pypi_0
wandb=0.15.7=pypi_0
wcwidth=0.2.6=pypi_0
werkzeug=2.2.3=pypi_0
wheel=0.38.4=py38h06a4308_0
xxhash=0.8.1=h0b41bf4_0
xz=5.2.10=h5eee18b_1
yaml=0.2.5=h7f98852_2
yarl=1.8.2=py38h0a891b7_0
zipp=3.13.0=pypi_0
zlib=1.2.13=h166bdaf_4
zstd=1.5.2=ha4553b6_0
