import argparse
import os

from trainer import Trainer
from utils import init_logger, load_tokenizer, read_prediction_text, set_seed, MODEL_CLASSES, MODEL_PATH_MAP
from data_loader import load_and_cache_examples
from torch.utils.tensorboard import SummaryWriter

def main(args):
    init_logger()
    set_seed(args)
    tokenizer = load_tokenizer(args)

    res_train = dict()
    res_dev = dict()
    res_test = dict()

    train_dataset = load_and_cache_examples(args, tokenizer, mode="train")
    test_dataset = load_and_cache_examples(args, tokenizer, mode="test")
    if args.test_as_dev is False:
        dev_dataset = load_and_cache_examples(args, tokenizer, mode="dev")
    else:
        dev_dataset = test_dataset

    if args.tb:
        tb = SummaryWriter(args.tb_dir)
        trainer = Trainer(args, train_dataset, dev_dataset, test_dataset, tb)
    else:
        trainer = Trainer(args, train_dataset, dev_dataset, test_dataset)

    if args.do_train:
        if args.prev_model_dir != None:
            trainer.load_previous_model()
        trainer.train()
        res_train = trainer.evaluate("train")
        res_dev = trainer.evaluate("dev")

    if args.do_eval:
        trainer.load_model()
        res_test = trainer.evaluate("test")

    if args.tb:
        tb.close()

    print("\n--- Results for lateX ---\n")
    if args.do_train :
        print("Set ", end='')
        for key in sorted(res_train.keys()):
            print("& {} ".format(key), end='')
        print("\\\\\n{}".format(args.seed), end=', ')        
        print("Train ", end='')
        res_train["sementic_frame_acc"] *= 100
        for key in sorted(res_train.keys()):
            print("& {:.2f} ".format(res_train[key]), end='')
        print("\\\\\n{}".format(args.seed), end=', ')
        res_dev["sementic_frame_acc"] *= 100
        print("Dev ", end='')
        for key in sorted(res_dev.keys()):
            print("& {:.2f} ".format(res_dev[key]), end='')
        print("\\\\\n{}".format(args.seed), end=', ')
    if args.do_eval:
        res_test["sementic_frame_acc"] *= 100
        print("Test ", end='')
        for key in sorted(res_test.keys()):
            print("& {:.2f} ".format(res_test[key]), end='')
    print("\\\\", flush=True)

    if not os.path.exists("results") or not os.path.isdir("results"):
        os.mkdir("results")

    with open(f"results/{args.task}_{args.model_type}", 'a') as f:
        if args.do_train :
            f.write("Seed, Set ")
            for key in sorted(res_train.keys()):
                f.write("& {} ".format(key))
            f.write("\\\\\n{}, ".format(args.seed))        
            f.write("Train ")
            res_train["sementic_frame_acc"] *= 100
            for key in sorted(res_train.keys()):
                f.write("& {:.2f} ".format(res_train[key]))
            f.write("\\\\\n{}, ".format(args.seed))
            res_dev["sementic_frame_acc"] *= 100
            f.write("Dev ")
            for key in sorted(res_dev.keys()):
                f.write("& {:.2f} ".format(res_dev[key]))
            f.write("\\\\\n{}, ".format(args.seed))
        if args.do_eval:
            res_test["sementic_frame_acc"] *= 100
            f.write("Test ")
            for key in sorted(res_test.keys()):
                f.write("& {:.2f} ".format(res_test[key]))
            f.write("\\\\\n")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Task to perform and transformer to use
    parser.add_argument("--task", default=None, required=True, type=str, help="The name of the task to train")
    parser.add_argument("--model_type", default="bert", type=str, help="Model type selected in the list: " + ", ".join(MODEL_CLASSES.keys()))
    parser.add_argument("--multilabel", action="store_true", help="If intent use a multilabel system")

    # Files and directories paths    
    parser.add_argument("--model_dir", default=None, required=True, type=str, help="Path to save, load model")
    parser.add_argument("--prev_model_dir", default=None, type=str, help="Path of model to load if using local file with joint architecture")
    parser.add_argument("--tokenizer_dir", default=None, help="Path of tokenizer to load if not default")
    parser.add_argument("--data_dir", default="./data", type=str, help="The input data dir")
    parser.add_argument("--intent_label_file", default="intent_label.txt", type=str, help="Intent Label file")
    parser.add_argument("--slot_label_file", default="slot_label.txt", type=str, help="Slot Label file")

    # Fixed parameters
    parser.add_argument('--seed', type=int, default=1234, help="random seed for initialization")
    parser.add_argument("--max_seq_len", default=50, type=int, help="The maximum total input sequence length after tokenization.")
    parser.add_argument("--slot_pad_label", default="PAD", type=str, help="Pad token for slot label pad (to be ignore when calculate loss)")
    parser.add_argument("--ignore_index", default=0, type=int,
                        help='Specifies a target value that is ignored and does not contribute to the input gradient')
    parser.add_argument('--slot_loss_coef', type=float, default=1.0, help='Coefficient for the slot loss.')

    # Hyperparameters
    parser.add_argument("--train_batch_size", default=32, type=int, help="Batch size for training.")
    parser.add_argument("--eval_batch_size", default=64, type=int, help="Batch size for evaluation.")
    parser.add_argument("--learning_rate", default=5e-5, type=float, help="The initial learning rate for Adam.")
    parser.add_argument("--adam_epsilon", default=1e-8, type=float, help="Epsilon for Adam optimizer.")
    parser.add_argument("--weight_decay", default=0.0, type=float, help="Weight decay if we apply some.")
    parser.add_argument("--num_train_epochs", default=10.0, type=float, help="Total number of training epochs to perform.")
    parser.add_argument('--gradient_accumulation_steps', type=int, default=1,
                        help="Number of updates steps to accumulate before performing a backward/update pass.")
    parser.add_argument("--max_grad_norm", default=1.0, type=float, help="Max gradient norm.")
    parser.add_argument("--max_steps", default=-1, type=int, help="If > 0: set total number of training steps to perform. Override num_train_epochs.")
    parser.add_argument("--warmup_steps", default=0, type=int, help="Linear warmup over warmup_steps.")
    parser.add_argument("--dropout_rate", default=0.1, type=float, help="Dropout for fully-connected layers")

    # Training and evaluating options
    parser.add_argument("--do_train", action="store_true", help="Whether to run training.")
    parser.add_argument("--do_eval", action="store_true", help="Whether to run eval on the test set.")
    parser.add_argument("--test_as_dev", action="store_true", help="Use test data as dev data for evalutation while training.")
    parser.add_argument("--no_cuda", action="store_true", help="Avoid using CUDA when available.")

    # Logging and saving options
    parser.add_argument('--logging_steps', type=int, default=200, help="Log every X updates steps.")
    parser.add_argument('--save_steps', type=int, default=200, help="Save checkpoint every X updates steps.")

    # CRF option
    parser.add_argument("--use_crf", action="store_true", help="Whether to use CRF")

    # Tensorboard option
    parser.add_argument("--tb", action="store_true", help="Save metrics in tensorboard for visualization.")
    parser.add_argument("--tb_dir", default="runs", type=str, help="Dir for tensorboard logs.")

    args = parser.parse_args()

    args.model_name_or_path = MODEL_PATH_MAP[args.model_type]
    args.abs_path = os.path.abspath(".")

    main(args)
