import os
import random
import logging
import time

import torch
import numpy as np
from seqeval.metrics import precision_score, recall_score, f1_score
from torchmetrics.functional.text import word_error_rate as wer

from transformers import BertConfig, DistilBertConfig, AlbertConfig, CamembertConfig
from transformers import BertTokenizer, DistilBertTokenizer, AlbertTokenizer, CamembertTokenizer

from model import JointBERT, JointDistilBERT, JointAlbert, JointCamembert

from ray import tune
from ray.tune import Stopper

MODEL_CLASSES = {
    'bert': (BertConfig, JointBERT, BertTokenizer),
    'distilbert': (DistilBertConfig, JointDistilBERT, DistilBertTokenizer),
    'albert': (AlbertConfig, JointAlbert, AlbertTokenizer),
    'fralbert': (AlbertConfig, JointAlbert, AlbertTokenizer),
    'camembert': (CamembertConfig, JointCamembert, CamembertTokenizer),
    'camembert-large': (CamembertConfig, JointCamembert, CamembertTokenizer),
    'camembert-ccnet': (CamembertConfig, JointCamembert, CamembertTokenizer),
    'camembert-wiki-4gb': (CamembertConfig, JointCamembert, CamembertTokenizer),
    'camembert-oscar-4gb': (CamembertConfig, JointCamembert, CamembertTokenizer),
    'camembert-ccnet-4gb': (CamembertConfig, JointCamembert, CamembertTokenizer),
}

MODEL_PATH_MAP = {
    'bert': 'bert-base-uncased',
    'distilbert': 'distilbert-base-uncased',
    'albert': 'albert-base-v2',
    'fralbert': 'qwant/fralbert-base',
    'camembert': 'camembert-base',
    'camembert-large': 'camembert/camembert-large',
    'camembert-ccnet': 'camembert/camembert-base-ccnet',
    'camembert-wiki-4gb': 'camembert/camembert-base-wikipedia-4gb',
    'camembert-oscar-4gb': 'camembert/camembert-base-oscar-4gb',
    'camembert-ccnet-4gb': 'camembert/camembert-base-ccnet-4gb',
}


def get_intent_labels(args):
    return [label.strip() for label in open(os.path.join(args.abs_path, args.data_dir, args.task, args.intent_label_file), 'r', encoding='utf-8')]


def get_slot_labels(args):
    return [label.strip() for label in open(os.path.join(args.abs_path, args.data_dir, args.task, args.slot_label_file), 'r', encoding='utf-8')]


def load_tokenizer(args):
    if args.tokenizer_dir == None:
        return MODEL_CLASSES[args.model_type][2].from_pretrained(args.model_name_or_path)
    else:
        return MODEL_CLASSES[args.model_type][2].from_pretrained(os.path.join(args.abs_path, args.tokenizer_dir))


def init_logger():
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
                        datefmt='%m/%d/%Y %H:%M:%S',
                        level=logging.INFO)


def set_seed(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if not args.no_cuda and torch.cuda.is_available():
        torch.cuda.manual_seed_all(args.seed)


def compute_metrics(intent_preds, intent_labels, slot_preds, slot_labels, multilabel=False):
    assert len(intent_preds) == len(intent_labels) == len(slot_preds) == len(slot_labels)
    results = {}
    intent_result = get_intent_acc(intent_preds, intent_labels, multilabel=multilabel)
    slot_result = get_slot_metrics(slot_preds, slot_labels)
    slot_cer = get_slot_cer(slot_preds, slot_labels)
    sementic_result = get_sentence_frame_acc(intent_preds, intent_labels, slot_preds, slot_labels, multilabel=multilabel)

    results.update(intent_result)
    results.update(slot_result)
    results.update(slot_cer)
    results.update(sementic_result)

    return results


def get_slot_cer(preds, labels):
    assert len(preds) == len(labels)
    sent_preds = []
    sent_labels = []
    for pred, lab in zip(preds, labels):
        sent_preds.append(" ".join([slot[2:] for slot in pred if slot[0] == "B"]))
        sent_labels.append(" ".join([slot[2:] for slot in lab if slot[0] == "B"]))
    return {
        "slot_cer": 100*(wer(sent_preds, sent_labels).item())
    }


def get_slot_metrics(preds, labels):
    assert len(preds) == len(labels)
    return {
        "slot_precision": 100*precision_score(labels, preds),
        "slot_recall": 100*recall_score(labels, preds),
        "slot_f1": 100*f1_score(labels, preds)
    }


def get_intent_acc(preds, labels, multilabel=False):
    if multilabel:
        emr = np.all(preds == labels, axis=1).mean()
        acc = 0
        for i in range(labels.shape[0]):
            acc += sum(np.logical_and(labels[i], preds[i])) / sum(np.logical_or(labels[i], preds[i]))
        acc = acc / labels.shape[0]
        return {
            "intent_acc": 100*acc,
            "intent_emr": 100*emr,
        }
    else:
        acc = (preds == labels).mean()
        return {
            "intent_acc": 100*acc
        }


def read_prediction_text(args):
    return [text.strip() for text in open(os.path.join(args.abs_path, args.pred_dir, args.pred_input_file), 'r', encoding='utf-8')]


def get_sentence_frame_acc(intent_preds, intent_labels, slot_preds, slot_labels, multilabel=False):
    """For the cases that intent and all the slots are correct (in one sentence)"""
    # Get the intent comparison result
    if multilabel:
        intent_result = np.all(intent_preds == intent_labels, axis=1)
    else:
        intent_result = (intent_preds == intent_labels)

    # Get the slot comparision result
    slot_result = []
    for preds, labels in zip(slot_preds, slot_labels):
        assert len(preds) == len(labels)
        one_sent_result = True
        for p, l in zip(preds, labels):
            if p != l:
                one_sent_result = False
                break
        slot_result.append(one_sent_result)
    slot_result = np.array(slot_result)

    sementic_acc = np.multiply(intent_result, slot_result).mean()
    return {
        "sementic_frame_acc": sementic_acc
    }

def set_pbt_config(args):
    """For population based training, will set config, hyperparam_mutations and early_stopping"""
    # Initial configurations for model, passed to train_joint function
    config = {
        "weight_decay": args.weight_decay,
        "learning_rate": args.learning_rate,
        "train_batch_size": args.train_batch_size,
        "eval_batch_size": args.eval_batch_size,
        "gradient_accumulation_steps": args.gradient_accumulation_steps,
        "adam_epsilon": args.adam_epsilon,
        "max_grad_norm": args.max_grad_norm,
        "dropout_rate": args.dropout_rate,
        "epochs": -1,
    }

    # Hyperparameters to explore with PBT, will modify config dictionnary
    hyperparam_mutations = dict()
    parameters_to_display = dict()
    if args.weight_decay == -1:
        hyperparam_mutations["weight_decay"] = tune.uniform(0.0, 0.3)
        parameters_to_display["weight_decay"] = "w_decay"
        del config["weight_decay"]
    if args.learning_rate == -1:
        hyperparam_mutations["learning_rate"] = tune.uniform(1e-5, 5e-5)
        parameters_to_display["learning_rate"] = "lr"
        del config["learning_rate"]
    if args.train_batch_size == -1:
        hyperparam_mutations["train_batch_size"] = [8, 16, 32]
        parameters_to_display["train_batch_size"] = "tr_bs"
        del config["train_batch_size"]
    if args.adam_epsilon == -1:
        hyperparam_mutations["adam_epsilon"] = tune.uniform(1e-8, 1e-6)
        parameters_to_display["adam_epsilon"] = "eps"
        del config["adam_epsilon"]
    if args.dropout_rate == -1:
        hyperparam_mutations["dropout_rate"] = tune.uniform(0.0, 0.7)
        parameters_to_display["dropout_rate"] = "dr"
        del config["dropout_rate"]

    # Get early stopping conditions in dictionnary for tune function
    early_stopping = dict()
    if args.stop_threshold > 0 :
        early_stopping["val_joint_loss"] = args.stop_threshold
    if args.stop_iterations > 0:
        early_stopping["training_iteration"] = args.stop_iterations
    if args.min_epochs > 0 and args.max_epochs > 0:
        early_stopping["training_iteration"] = tune.randint(args.min_epochs, args.max_epochs + 1)
    if len(early_stopping) == 0:
        early_stopping = None

    if args.stop_time_min > 0:
        args.stop_time_min = args.stop_time_min * 60
    else:
        args.stop_time_min = None

    return args, config, hyperparam_mutations, parameters_to_display, early_stopping
