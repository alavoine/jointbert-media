import argparse
import os
import torch
import logging

from trainer_pbt import Trainer, train_joint, evaluate
from utils import init_logger, load_tokenizer, read_prediction_text, set_seed, MODEL_CLASSES, MODEL_PATH_MAP, set_pbt_config
from data_loader import load_and_cache_examples

# Ray imports for PBT
import ray
from ray import tune
from ray import air
from ray.tune import CLIReporter, Stopper
from ray.tune.schedulers import PopulationBasedTraining

from functools import partial

def main(args):
    # Initialize ray for population based training
    if args.do_train and args.slurm == True:
        ray.init(address="auto", _node_ip_address=os.environ["ip_head"].split(":")[0], \
                _redis_password=os.environ["redis_password"])
    elif args.do_train :
        if ray.is_initialized():
            ray.shutdown()
        ray.init(num_gpus=args.gpu, num_cpus=args.cpu)

    res_train = dict()
    res_dev = dict()
    res_test = dict()

    # Logger, seed and tokenizer initialization
    init_logger()
    set_seed(args)
    print(os.getcwd())
    tokenizer = load_tokenizer(args)        
    logger = logging.getLogger(__name__)
    if args.tokenizer_dir != None:
        print(f"Tokenizer loaded from {args.tokenizer_dir}")

    # Load datasets
    train_dataset = load_and_cache_examples(args, tokenizer, mode="train")
    dev_dataset = load_and_cache_examples(args, tokenizer, mode="dev")
    test_dataset = load_and_cache_examples(args, tokenizer, mode="test")

    train_ds_id = ray.put(train_dataset)
    dev_ds_id = ray.put(dev_dataset)
    test_ds_id = ray.put(test_dataset)
    
    # Initialize and load trainer
    args, config, hyperparam_mutations, parameters_to_display, early_stopping = set_pbt_config(args)

    #### For training 
    if args.do_train:

        # Check that there's a least one hyperparameter to optimize
        if len(hyperparam_mutations) == 0:
            print("/!\ At least one hyperparameter must be explored for PBT")
            print("Training not performed")
            exit()        

        # Population Based Training scheduler
        scheduler = PopulationBasedTraining(
            hyperparam_mutations=hyperparam_mutations, # tested hyperparameters
            time_attr="training_iteration", # interval for perturbations
            metric="val_opti_metric", # metric to evaluate performance
            mode="max", # metric must be minimize (min) or maximized (max)
            perturbation_interval=args.perturb_interval, # interval to test new hyperparameters
            # synch=True # Can be uncommented for easier visualization (but slower!)
        )

        # Reporter for log, included in ray library
        if args.multilabel:
            reporter = CLIReporter(
                parameter_columns=parameters_to_display,
                metric_columns=["val_opti_metric", "val_slot_f1", "val_slot_cer",
                                "val_intent_acc", "val_intent_emr",
                                "val_sem_fr_acc", "training_iteration"],
                metric="val_opti_metric",
                mode="max",
                max_report_frequency=240,
            )
        else:
            reporter = CLIReporter(
                parameter_columns=parameters_to_display,
                metric_columns=["val_opti_metric", "val_slot_f1",
                                "val_intent_acc", "val_slot_cer",
                                "val_sem_fr_acc", "training_iteration"],
                metric="val_opti_metric",
                mode="max",
                max_report_frequency=240,
            )            

        ### NEW Ray Tune API
        if args.slurm:
            to_tune = tune.with_resources(
                partial(train_joint, args=args, train_dataset=train_ds_id, dev_dataset=dev_ds_id, test_dataset=test_ds_id),
                {"cpu": args.cpu, "gpu": args.gpu}, #resources per trial
            )
            checkpoints_to_keep=None
        else:
            to_tune = tune.with_resources(
                partial(train_joint, args=args, train_dataset=train_ds_id, dev_dataset=dev_ds_id, test_dataset=test_ds_id),
                {"gpu": args.gpu}
            )
            checkpoints_to_keep=3

        exp_name = str(args.task + "_joint_" + args.model_type + "_seed_" + str(args.seed)) if args.name == None else args.name

        print(f"Experience will be saved under ray_results/{exp_name}")

        tuner = tune.Tuner(
            to_tune,
            param_space=config,
            run_config=air.RunConfig(
                name=exp_name,
                local_dir="./ray_results", # dir for checkpoints
                log_to_file=True, # to save log
                checkpoint_config=air.CheckpointConfig(
                    checkpoint_score_attribute="val_opti_metric",
                    # checkpoint_at_end=True,
                    num_to_keep=checkpoints_to_keep,
                    #checkpoint_frequency=50,
                ),
                #sync_config=tune.SyncConfig(syncer=None), # this is necessary for slurm to syncing checkpoints between nodes
                stop=early_stopping, # early stopping conditions
                progress_reporter=reporter,
            ),
            tune_config=tune.TuneConfig(
                scheduler=scheduler,
                num_samples=args.num_samples, # number of parallels PBT tested
                time_budget_s=args.stop_time_min, # to stop experiment after said time
            ),
        )

        results = tuner.fit()
        ray.shutdown()

        # Get best trial and recopy args
        best_result = results.get_best_result(metric="val_opti_metric", mode="min")
        args.train_batch_size = best_result.config["train_batch_size"]
        args.eval_batch_size = best_result.config["eval_batch_size"]
        args.weight_decay = best_result.config["weight_decay"]
        args.learning_rate = best_result.config["learning_rate"]
        args.dropout_rate = best_result.config["dropout_rate"]
        args.adam_epsilon = best_result.config["adam_epsilon"]
        args.max_grad_norm = best_result.config["max_grad_norm"]
        args.gradient_accumulation_steps = best_result.config["gradient_accumulation_steps"]
        
        logger.info("Best trial config: {}".format(best_result.config))
        logger.info("Best trial final validation opti metric (mean of f1 slot and intent accuracy): {}".format(best_result.metrics["val_opti_metric"]))

        # Initialize new trainer with best_result args and copy best_result weights inside
        best_checkpoint = (best_result.checkpoint).to_dict()
        trainer = Trainer(args, train_dataset, dev_dataset, test_dataset)
        trainer.model.load_state_dict(best_checkpoint["model_state_dict"])

        # Save it !
        trainer.save_model()
        res_train = evaluate(config, trainer, train_dataset, "train", multilabel=args.multilabel)
        res_dev = evaluate(config, trainer, dev_dataset, "dev", multilabel=args.multilabel)

    ### For evaluating
    if args.do_eval:
        trainer = Trainer(args, train_dataset, dev_dataset, test_dataset)
        trainer.load_model()
        res_test = evaluate(config, trainer, test_dataset, "test", multilabel=args.multilabel)

    print("\n--- Results for lateX ---\n")
    if args.do_train :
        print("Seed, Set ", end='')
        for key in sorted(res_train.keys()):
            print("& {} ".format(key), end='')
        print("\\\\\n{}".format(args.seed), end=', ')        
        print("Train ", end='')
        res_train["sementic_frame_acc"] *= 100
        for key in sorted(res_train.keys()):
            print("& {:.2f} ".format(res_train[key]), end='')
        print("\\\\\n{}".format(args.seed), end=', ')
        res_dev["sementic_frame_acc"] *= 100
        print("Dev ", end='')
        for key in sorted(res_dev.keys()):
            print("& {:.2f} ".format(res_dev[key]), end='')
        print("\\\\\n{}".format(args.seed), end=', ')
    if args.do_eval:
        res_test["sementic_frame_acc"] *= 100
        print("Test ", end='')
        for key in sorted(res_test.keys()):
            print("& {:.2f} ".format(res_test[key]), end='')
    print("\\\\", flush=True)

    if not os.path.exists("results") or not os.path.isdir("results"):
        os.mkdir("results")

    with open(f"results/{args.task}_{args.model_type}_pbt", 'a') as f:
        if args.do_train :
            f.write("Seed, Set ")
            for key in sorted(res_train.keys()):
                f.write("& {} ".format(key))
            f.write("\\\\\n{}, ".format(args.seed))        
            f.write("Train ")
            for key in sorted(res_train.keys()):
                f.write("& {:.2f} ".format(res_train[key]))
            f.write("\\\\\n{}, ".format(args.seed))
            f.write("Dev ")
            for key in sorted(res_dev.keys()):
                f.write("& {:.2f} ".format(res_dev[key]))
            f.write("\\\\\n{}, ".format(args.seed))
        if args.do_eval:
            f.write("Test ")
            for key in sorted(res_test.keys()):
                f.write("& {:.2f} ".format(res_test[key]))
            f.write("\\\\\n")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Task to perform and transformer to use
    parser.add_argument("--task", default=None, required=True, type=str, help="The name of the task to train")
    parser.add_argument("--model_type", default="bert", type=str, help="Model type selected in the list: " + ", ".join(MODEL_CLASSES.keys()))
    parser.add_argument("--name", default=None, type=str, help="Experience name")
    parser.add_argument("--multilabel", action="store_true", help="If intent use a multilabel system")

    # Files and directories paths
    parser.add_argument("--model_dir", default=None, required=True, type=str, help="Path to save, load model")
    parser.add_argument("--prev_model_dir", default=None, type=str, help="Path of model to load if using local file with joint architecture")
    parser.add_argument("--tokenizer_dir", default=None, help="Path of tokenizer to load if not default")
    parser.add_argument("--data_dir", default="./data", type=str, help="The input data dir")
    parser.add_argument("--intent_label_file", default="intent_label.txt", type=str, help="Intent Label file")
    parser.add_argument("--slot_label_file", default="slot_label.txt", type=str, help="Slot Label file")

    # Some values for PBT training
    parser.add_argument("--num_samples", default=3, type=int, help="Number of trials to run in parallels.")
    parser.add_argument("--perturb_interval", default=1, type=int, help="Interval for perturbations of trials (seeking new hyperparameters values).")

    # Fixed parameters
    parser.add_argument('--seed', type=int, default=1234, help="Random seed for initialization.")
    parser.add_argument("--max_seq_len", default=50, type=int, help="The maximum total input sequence length after tokenization.")
    parser.add_argument("--slot_pad_label", default="PAD", type=str, help="Pad token for slot label pad (to be ignore when calculate loss)")
    
    # Hyperparameters, can be explored with PBT
    parser.add_argument("--train_batch_size", default=32, type=int, help="Batch size for training. If set to -1, will be searched by PBT inside [8, 16, 32].")
    parser.add_argument("--learning_rate", default=5e-5, type=float, help="The initial learning rate for Adam. If set to -1, will be searched by PBT between 1e-5 and 5e-5.")
    parser.add_argument("--adam_epsilon", default=1e-8, type=float, help="Epsilon for Adam optimizer. If set to -1, will be searched by PBT between 1e-6 and 1e-8.")
    parser.add_argument("--weight_decay", default=0.0, type=float, help="Weight decay if we apply some. If set to -1, will be searched with PBT between 0.1 and 0.3.")
    parser.add_argument("--dropout_rate", default=0.1, type=float, help="Dropout for fully-connected layers. If set to -1, will be searched with PBT between 0.0 and 0.7.")
    parser.add_argument("--min_epochs", default=-1, type=int, help="If set > 0, epochs will be explore in an interval starting from this value. --max_epochs must be activated. Supersed stop_iterations if activated.")
    parser.add_argument("--max_epochs", default=-1, type=int, help="If set > 0, epochs will be explore in an interval finishing at this value. --min_epochs must be activated. Supersed stop_iterations if activated.")
    parser.add_argument("--eval_batch_size", default=64, type=int, help="Batch size for evaluation.")
    parser.add_argument('--gradient_accumulation_steps', type=int, default=1, help="Number of updates steps to accumulate before performing a backward/update pass.")
    parser.add_argument("--max_grad_norm", default=1.0, type=float, help="Max gradient norm.")

    # CRF option
    parser.add_argument("--use_crf", action="store_true", help="Whether to use CRF")

    # Training and evaluating options
    parser.add_argument("--do_train", action="store_true", help="Whether to run training.")
    parser.add_argument("--do_eval", action="store_true", help="Whether to run eval on the test set.")
    parser.add_argument("--no_cuda", action="store_true", help="Avoid using CUDA when available.")
    parser.add_argument("--no_dev", action="store_true", help="Use 10% of training data as dev data for evalutation while training and choosing best model.")
    parser.add_argument("--cpu", type=int, default=1, help="Number of cpu requested. 1 by default.")
    parser.add_argument("--gpu", type=int, default=1, help="Number of gpu requested. 1 by default, or 0 if no_cuda option is activated.")

    # Early stopping
    parser.add_argument("--stop_threshold", default=-1, type=float, help="If set > 0, will stop PBT when evaluation loss is under threshold.")
    parser.add_argument("--stop_iterations", default=-1, type=int, help="If set > 0, will stop PBT when a total of set iterations is reached.")
    parser.add_argument("--stop_time_min", default=-1, type=int, help="If set > 0, will stop PBT after a total of set time (in minutes) is reached.")

    # Others
    parser.add_argument("--slurm", action="store_true", help="Whether to run training with SLURM cluster manager.")
    parser.add_argument("--ignore_index", default=0, type=int, help='Specifies a target value that is ignored and does not contribute to the input gradient')
    parser.add_argument('--slot_loss_coef', type=float, default=1.0, help='Coefficient for the slot loss.')

    args = parser.parse_args()

    args.abs_path = os.path.abspath(".")
    args.model_name_or_path = MODEL_PATH_MAP[args.model_type]
    args.gpu = args.gpu if args.no_cuda == False else 0

    if args.max_epochs > 0 or args.min_epochs > 0:
        assert args.max_epochs > 0
        assert args.min_epochs > 0
        assert args.max_epochs > args.min_epochs

    os.environ["TUNE_MAX_PENDING_TRIALS_PG"] = "1"

    main(args)
